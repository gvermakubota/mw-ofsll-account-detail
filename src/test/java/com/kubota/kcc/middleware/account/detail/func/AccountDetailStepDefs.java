package com.kubota.kcc.middleware.account.detail.func;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;

@AutoConfigureMockMvc
@SpringBootTest
@ActiveProfiles("dev")
public class AccountDetailStepDefs {
	private String accountId;
	private String serviceURI = "/account/detail/";
	private int respStatusCode;
	
	@Autowired
    private MockMvc mockMvc;
	
	@Value("#{'${ofsll.legal.status.codes}'.split(',')}")
	private List<String> ofsllLegalStatusCodes;
	
	
	
	@Given("an account exists with an account ID of {string}")
	public void an_account_exists_with_an_account_ID_of(String string) {
		accountId = string;
	}
	
	@Given("an account does not exist with an account ID of {string}")
	public void an_account_does_not_exists_with_an_account_ID_of(String string) {
		accountId = string;
	}
	
	@When("a system retrieves the account by account ID")
	public void a_system_retrieves_the_account_by_account_ID() throws Exception {
		MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(serviceURI + accountId)
				  .accept(MediaType.APPLICATION_JSON))
				  .andDo(print())				  
				  .andReturn();
		respStatusCode = result.getResponse().getStatus();
	}

	@Then("the status code is {int}")
	public void the_status_code_is(int statusCode) {
	    assertEquals(statusCode,respStatusCode);
	}
	
	
	@And("contains legal condition codes")
	public void contains_legal_condition_codes() throws Exception {
		return;
	}
	
	@And("does not contains legal condition codes")
	public void does_not_contain_legal_condition_codes() throws Exception {
		return;
	}
	
	@Then("the response should contain a custom element indicating legal status is {string}")
	public void the_response_should_contain_a_custom_element_indicating_legal_status_is(String string) throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get(serviceURI + accountId)
				  .accept(MediaType.APPLICATION_JSON))
				  .andDo(print())				  
				  .andExpect(jsonPath("$.AccountDetailResponse.AccountDetailSummary[0].Custom.StringData[0].KeyName").value("LEGAL_STATUS"))
				  .andExpect(jsonPath("$.AccountDetailResponse.AccountDetailSummary[0].Custom.StringData[0].KeyValue").value(string));
	}
	
	@Given("a valid account in CHGOFF or VOID status of {string}")
	public void a_valid_account_in_CHGOFF_or_VOID_status(String string) throws Exception {
		accountId = string;
	}	
	
	@And("the account is in PAIDOFF status")
	public void the_account_is_in_PAIDOFF_status() throws Exception {
		return;
	}
	
	@And("the account was paid off less than 270 days from current date")
	public void the_account_was_paid_off_less_than_270_days_from_current_date() throws Exception {
		return;
	}
	
	@Then("return the account details and {int} response")
	public void return_the_account_details_and_response_code(int statusCode) throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get(serviceURI + accountId)
				  							  .accept(MediaType.APPLICATION_JSON))
				  							  .andDo(print())
				  							  .andExpect(status().isOk());
	}

}
