package com.kubota.kcc.middleware.account.detail.service;


import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountDetailResponse;


@SpringBootTest
public class AccountDetailResponseErrorHandlerTest {
	@Autowired
	AccountDetailResponseErrorHandler handler;
	
	@Mock
	ClientHttpResponse response;
			
      
    @Autowired
    private RestTemplate restTemplate;
	
	@Test
	public void hasErrorReturnsTrueForHTTP400() throws Exception {		
		Mockito.when(response.getStatusCode()).thenReturn(HttpStatus.BAD_REQUEST);
		assertEquals(true, handler.hasError(response));
	}
	
	@Test
	public void hasErrorReturnsTrueForHTTP500() throws Exception {		
		Mockito.when(response.getStatusCode()).thenReturn(HttpStatus.INTERNAL_SERVER_ERROR);
		assertEquals(true, handler.hasError(response));
	}
	
	@Test
	public void handleErrorDoesNotThrowExceptionForHTTP500() throws Exception {		
		MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
		server.expect(ExpectedCount.once(), requestTo("/account/12345"))
				   .andExpect(method(HttpMethod.GET))
		           .andRespond(withStatus(HttpStatus.INTERNAL_SERVER_ERROR));
		
		AccountDetailResponse response = restTemplate.getForObject("/account/12345", AccountDetailResponse.class);
		server.verify();
	}
	
	@Test
	public void handleErrorDoesNotThrowExceptionForHTTP400() throws Exception {		
		MockRestServiceServer server = MockRestServiceServer.bindTo(restTemplate).build();
		server.expect(ExpectedCount.once(), requestTo("/account/12345"))
				   .andExpect(method(HttpMethod.GET))
		           .andRespond(withStatus(HttpStatus.BAD_REQUEST));
		
		AccountDetailResponse response = restTemplate.getForObject("/account/12345", AccountDetailResponse.class);
		server.verify();
	}
}
