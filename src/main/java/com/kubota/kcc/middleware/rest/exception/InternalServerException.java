package com.kubota.kcc.middleware.rest.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class InternalServerException extends RuntimeException {
	private static final long serialVersionUID = -5112566524754855037L;

	final String accountId;
	
	public InternalServerException(String message, String accountId) {
		super(message);
		this.accountId = accountId;
	}

	public String getAccountId() {
		return accountId;
	}
}
