package com.kubota.kcc.middleware.account.detail.util;

public class Constants {
	private Constants() {
		throw new IllegalStateException("Utility class");

	}
	
	public static final String ACCOUNT_BALANCE_TRANS_TYPE_INTEREST = "INT";
	
	public static final String ACCOUNT_STATUS_CHGOFF = "CHGOFF";
	public static final String ACCOUNT_STATUS_PAID = "PAID";	
	public static final String ACCOUNT_STATUS_VOID = "VOID";
	
	public static final String FUTURE_PMT_DT_DATE_DATA_KEY_NAME = "FUTURE_PMT_DT";
	public static final String OLDEST_DUE_DT_DATE_DATA_KEY_NAME = "OLDEST_DUE_DT";
	public static final String PAID_OFF_DT_DATE_DATA_KEY_NAME = "PAID_OFF_DT";
	
	
	public static final String FUTURE_PMT_AMT_NUMBER_KEY_NAME = "FUTURE_PMT_AMT";
	
	public static final String LEGAL_STATUS_STR_DATA_KEY_NAME = "LEGAL_STATUS";
	
	public static final String N_STR_DATA_KEY_VALUE = "N";
	public static final String Y_STR_DATA_KEY_VALUE = "Y";
	
	public static final String RESULT_ERROR_STATUS = "ERROR";
}
