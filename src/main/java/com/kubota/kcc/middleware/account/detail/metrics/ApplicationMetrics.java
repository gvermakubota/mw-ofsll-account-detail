package com.kubota.kcc.middleware.account.detail.metrics;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Component
public class ApplicationMetrics {

    @Autowired
    Logger appLogger;

    @Autowired
    MeterRegistry registry;

    public void logRequestCount() {
        Counter recordCounter = registry.counter("ofsll.account.detail.requests");
        recordCounter.increment();
    }
    
    public void logOfsllResponseTime(long startTime) {
        Timer timer = registry.timer("ofsll.account.detail.response.time");
        long elapsedTime = System.nanoTime() - startTime;
        long elapsedTimeInSecond = elapsedTime / 1_000_000_000;
        appLogger.trace("OFSLL Account Detail Response Time: [{}]", elapsedTimeInSecond);
        timer.record(elapsedTimeInSecond, TimeUnit.SECONDS);
    }
}
