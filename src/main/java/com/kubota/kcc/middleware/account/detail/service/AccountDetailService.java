package com.kubota.kcc.middleware.account.detail.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.kubota.kcc.middleware.account.detail.metrics.ApplicationMetrics;
import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountDetailResponse;
import com.kubota.kcc.middleware.account.detail.model.ofsll.AccountDetailResponse__1;
import com.kubota.kcc.middleware.account.detail.model.ofsll.Custom;
import com.kubota.kcc.middleware.account.detail.model.ofsll.DateDatum;
import com.kubota.kcc.middleware.account.detail.model.ofsll.Result;
import com.kubota.kcc.middleware.account.detail.util.Constants;
import com.kubota.kcc.middleware.rest.exception.InternalServerException;
import com.kubota.kcc.middleware.rest.exception.NotFoundException;


@Service
public class AccountDetailService {
	@Autowired
	Logger applogger;
	
	@Autowired
	ApplicationMetrics applicationMetrics;
	
	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	AccountDetailMapper mapper;

	@Value("${ofsll.endpoint.account.detail.url}")
	private String ofsllAccountDetailEndpoint;
	
	// Service method to retrieve Account Detail data from OFSLL
	public com.kubota.kcc.middleware.account.detail.model.rest.AccountDetailResponse getAccountDetail(String accountId) {
		ThreadContext.put("appName", "mw-ofsll-account-detail");
		ThreadContext.put("accountID", accountId);
		com.kubota.kcc.middleware.account.detail.model.rest.AccountDetailResponse response;
		
		long startTime = System.nanoTime();
		
		try {
			applicationMetrics.logRequestCount();
			applogger.trace("Account ID Request");			
			
			// AccountDetailResponseErrorHandler is called if any exception thrown from restTemplate
			response = processAccountDetailResponse(restTemplate.getForObject(ofsllAccountDetailEndpoint + accountId, AccountDetailResponse.class));
			
			applicationMetrics.logOfsllResponseTime(startTime);
		} catch (NotFoundException notFoundEx) { // Catch Not Found Exception from processAccountDetailResponse
			applicationMetrics.logOfsllResponseTime(startTime);
			applogger.error("OFSLL Account Not Found: [{}]", notFoundEx.getResponse().getAccountDetailResponse().getResult().getStatusDetails());
			applogger.trace("Account not found");
			throw notFoundEx;
		} catch (Exception ex) { // Catch any service errors, ie. timeout, npe
			applicationMetrics.logOfsllResponseTime(startTime);
			applogger.error("getAccountDetail() Exception: ", ex);
			applogger.fatal("getAccountDetail() Exception: ", ex);
			applogger.trace("getAccountDetail() Exception: ", ex);
			throw new InternalServerException(ex.getMessage(), accountId);
		}
		
		applogger.trace("Account ID Request Success");

		return response;
	}
	
	// Method to do any post-processing of OFSLL Account Detail data
	com.kubota.kcc.middleware.account.detail.model.rest.AccountDetailResponse processAccountDetailResponse(AccountDetailResponse response) {
		com.kubota.kcc.middleware.account.detail.model.rest.AccountDetailResponse restResponse = new com.kubota.kcc.middleware.account.detail.model.rest.AccountDetailResponse();

		if( response.getAccountDetailResponse().getResult().getStatus().equals("SUCCESS") && doNotSendAccountToPaymentus(response) )
				throw new NotFoundException(clearResponseData(response));
		else // Map OFSLL success and error responses
			restResponse = mapper.mapPaymentusAccountDetailResponse(response);
		
		return restResponse;		
	}
	
	/* 
	 * Method to remove account detail information from response and only provide a Result.
	 * To be used when account is in CHGOFF or VOID status
	 * */
	AccountDetailResponse clearResponseData(AccountDetailResponse inResponse) {
		AccountDetailResponse outResponse = new AccountDetailResponse();
		AccountDetailResponse__1 outResponse1 = new AccountDetailResponse__1();
		
		outResponse1.setAccountDetailRequest(inResponse.getAccountDetailResponse().getAccountDetailRequest());
		
		Result result = new Result();
		result.setStatus(Constants.RESULT_ERROR_STATUS);
		result.setStatusDetails("Account is in CHGOFF or VOID status");
		outResponse1.setResult(result);
		outResponse.setAccountDetailResponse(outResponse1);
		return outResponse;
	}
	
	/* 
	 * If account has following criteria we should not send to Paymentus
	 * Account Status: CHGOFF or VOID
	 * Account Status: PAIDOFF and Paidoff Date > Current Date - 270 Days
	 */
	boolean doNotSendAccountToPaymentus(AccountDetailResponse response) {
		String accountStatus = response.getAccountDetailResponse().getAccountDetailSummary().get(0).getAccountStatus();
		
		return ( 
				 accountStatus.equals(Constants.ACCOUNT_STATUS_CHGOFF) || 
				 accountStatus.equals(Constants.ACCOUNT_STATUS_VOID ) ||
				 dateOlderThanDays(getPaidoffDate(response), 270)
			   );
	}
	
	/* Retrieve that PAID_OFF_DT value, if available, from OFSLL response */
	String getPaidoffDate(AccountDetailResponse response) {
		Custom customObj = response.getAccountDetailResponse().getAccountDetailSummary().get(0).getCustom();
		
		if( customObj == null )
			return "";
				
		DateDatum paidOffDateDatum = customObj.getDateData().stream()
															.filter(dateData -> dateData.getKeyName().equals("PAID_OFF_DT"))
															.findFirst()
															.orElse(null);
		
		return (paidOffDateDatum != null ? paidOffDateDatum.getKeyValue() : "");
	}
	
	/* 
	 * Determine if date is older than specified number of days from current time
	 * 
	 */
	boolean dateOlderThanDays(String inDate, int days) {
		if( inDate == null || inDate.isEmpty() )
			return false;
		
		LocalDate now = LocalDate.now();
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S");
		LocalDate paidOffDate = LocalDate.parse(inDate, dtf);
		
		long diffInDays = ChronoUnit.DAYS.between(paidOffDate, now);
		
		return ( diffInDays > days );
	}	
}
