package com.kubota.kcc.middleware.account.detail;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryCustomizer;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.kubota.kcc.middleware.account.detail.service.RestTemplateGenerator;

import io.micrometer.core.instrument.MeterRegistry;

@SpringBootApplication
public class MwOfsllAccountDetailApplication {
	@Bean
	public Logger applogger() {
		return LogManager.getLogger("applogger");
	}
	
	@Bean
	public RestTemplateGenerator getRestTemplateGenerator() {
		return new RestTemplateGenerator();
	}
	
	@Bean
	public RestTemplate getRestTemplate() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, CertificateException, IOException {
		return getRestTemplateGenerator().restTemplate(); 
	}
		
	public static void main(String[] args) {
		SpringApplication.run(MwOfsllAccountDetailApplication.class, args);
	}
	
	@Bean
    MeterRegistryCustomizer<MeterRegistry> metricsCommonTags() {
		return registry -> registry.config().commonTags("application", "mw-ofsll-account-detail");
    }
}
